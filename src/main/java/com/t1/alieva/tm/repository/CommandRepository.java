package com.t1.alieva.tm.repository;

import com.t1.alieva.tm.api.repository.ICommandRepository;
import com.t1.alieva.tm.constant.ArgumentConst;
import com.t1.alieva.tm.constant.TerminalConst;
import com.t1.alieva.tm.model.Command;

public final class CommandRepository implements ICommandRepository {

    public static Command ABOUT = new Command(
            TerminalConst.ABOUT, ArgumentConst.ABOUT,
            "Show developer info."
    );

    public final static Command VERSION = new Command(
            TerminalConst.VERSION, ArgumentConst.VERSION,
            "Show application version."
    );

    public final static Command HELP = new Command(
            TerminalConst.HELP, ArgumentConst.HELP,
            "Show application commands."
    );

    public final static Command EXIT = new Command(
            TerminalConst.EXIT, null,
            "Close application."
    );

    private final static Command PROJECT_CLEAR = new Command(
            TerminalConst.PROJECT_CLEAR, null,
            "Remove all projects."
    );

    private final static Command PROJECT_LIST = new Command(
            TerminalConst.PROJECT_LIST, null,
            "Display project list.");

    private final static Command TASK_CREATE = new Command(
            TerminalConst.TASK_CREATE, null,
            "Create new task.");
    private final static Command TASK_CLEAR = new Command(
            TerminalConst.TASK_CLEAR, null,
            "Remove all tasks."
    );

    private final static Command TASK_LIST = new Command(
            TerminalConst.TASK_LIST, null,
            "Display task list.");

    private final static Command PROJECT_CREATE = new Command(
            TerminalConst.PROJECT_CREATE, null,
            "Create new project.");

    private final static Command PROJECT_SHOW_BY_ID = new Command(
            TerminalConst.PROJECT_SHOW_BY_ID, null,
            "Find Project by ID.");

    private final static Command PROJECT_SHOW_BY_INDEX = new Command(
            TerminalConst.PROJECT_SHOW_BY_INDEX, null,
            "Find Project by INDEX.");

    private final static Command PROJECT_REMOVE_BY_INDEX = new Command(
            TerminalConst.PROJECT_REMOVE_BY_INDEX, null,
            "Remove Project by INDEX.");

    private final static Command PROJECT_REMOVE_BY_ID = new Command(
            TerminalConst.PROJECT_REMOVE_BY_ID, null,
            "Remove Project by ID.");

    private final static Command PROJECT_UPDATE_BY_INDEX = new Command(
            TerminalConst.PROJECT_UPDATE_BY_INDEX, null,
            "Update Project by INDEX.");

    private final static Command PROJECT_UPDATE_BY_ID = new Command(
            TerminalConst.PROJECT_UPDATE_BY_ID, null,
            "Update Project by ID.");

    private final static Command PROJECT_START_BY_ID = new Command(
            TerminalConst.PROJECT_START_BY_ID, null,
            "Start Project by ID.");

    private final static Command PROJECT_START_BY_INDEX = new Command(
            TerminalConst.PROJECT_START_BY_INDEX, null,
            "Start Project by INDEX.");

    private final static Command PROJECT_CHANGE_BY_INDEX = new Command(
            TerminalConst.PROJECT_CHANGE_BY_INDEX, null,
            "Change Project by INDEX.");

    private final static Command PROJECT_CHANGE_BY_ID = new Command(
            TerminalConst.PROJECT_CHANGE_BY_ID, null,
            "Change Project by ID.");

    private final static Command PROJECT_COMPLETE_BY_INDEX = new Command(
            TerminalConst.PROJECT_COMPLETE_BY_INDEX, null,
            "Complete Project by INDEX.");

    private final static Command PROJECT_COMPLETE_BY_ID = new Command(
            TerminalConst.PROJECT_COMPLETE_BY_ID, null,
            "Complete Project by ID.");

    private final static Command TASK_SHOW_BY_ID = new Command(
            TerminalConst.TASK_SHOW_BY_ID, null,
            "Find Task by ID.");

    private final static Command TASK_SHOW_BY_INDEX = new Command(
            TerminalConst.TASK_SHOW_BY_INDEX, null,
            "Find Task by INDEX.");

    private final static Command TASK_REMOVE_BY_INDEX = new Command(
            TerminalConst.TASK_REMOVE_BY_INDEX, null,
            "Remove Task by INDEX.");

    private final static Command TASK_REMOVE_BY_ID = new Command(
            TerminalConst.TASK_REMOVE_BY_ID, null,
            "Remove Task by ID.");

    private final static Command TASK_UPDATE_BY_INDEX = new Command(
            TerminalConst.TASK_UPDATE_BY_INDEX, null,
            "Update Task by INDEX.");

    private final static Command TASK_UPDATE_BY_ID = new Command(
            TerminalConst.TASK_UPDATE_BY_ID, null,
            "Update Task by ID.");

    private final static Command TASK_START_BY_ID = new Command(
            TerminalConst.TASK_START_BY_ID, null,
            "Start Task by ID.");

    private final static Command TASK_START_BY_INDEX = new Command(
            TerminalConst.TASK_START_BY_INDEX, null,
            "Start Task by INDEX.");

    private final static Command TASK_CHANGE_BY_INDEX = new Command(
            TerminalConst.TASK_CHANGE_BY_INDEX, null,
            "Change Task by INDEX.");

    private final static Command TASK_CHANGE_BY_ID = new Command(
            TerminalConst.TASK_CHANGE_BY_ID, null,
            "Change Task by ID.");

    private final static Command TASK_COMPLETE_BY_INDEX = new Command(
            TerminalConst.TASK_COMPLETE_BY_INDEX, null,
            "Complete Task by INDEX.");

    private final static Command TASK_COMPLETE_BY_ID = new Command(
            TerminalConst.TASK_COMPLETE_BY_ID, null,
            "Complete Task by ID.");
    public final static Command INFO = new Command(
            TerminalConst.INFO, ArgumentConst.INFO,
            "Show information about device."
    );
    private final static Command TASK_BIND_TO_PROJECT= new Command(
            TerminalConst.TASK_BIND_TO_PROJECT, null,
            "Task bind to project.");

    private final static Command TASK_UNBIND_TO_PROJECT= new Command(
            TerminalConst.TASK_UNBIND_TO_PROJECT, null,
            "Task unbind to project.");

    private final static Command TASK_SHOW_BY_PROJECT_ID= new Command(
            TerminalConst.TASK_SHOW_BY_PROJECT_ID, null,
            "Task show by project id.");

    private final  static Command[] TERMINAL_COMMANDS = new Command[] {
            INFO, ABOUT, VERSION,

            PROJECT_CLEAR, PROJECT_LIST, PROJECT_CREATE,
            PROJECT_SHOW_BY_ID,PROJECT_SHOW_BY_INDEX,PROJECT_UPDATE_BY_ID,PROJECT_REMOVE_BY_INDEX,
            PROJECT_UPDATE_BY_INDEX,PROJECT_REMOVE_BY_ID,
            PROJECT_START_BY_ID,PROJECT_START_BY_INDEX,PROJECT_CHANGE_BY_ID,PROJECT_CHANGE_BY_INDEX,
            PROJECT_COMPLETE_BY_INDEX,PROJECT_COMPLETE_BY_ID,

            TASK_SHOW_BY_ID,TASK_SHOW_BY_INDEX,TASK_UPDATE_BY_ID,TASK_REMOVE_BY_INDEX,
            TASK_UPDATE_BY_INDEX,TASK_REMOVE_BY_ID,
            TASK_CLEAR, TASK_LIST, TASK_CREATE,
            TASK_START_BY_ID,TASK_START_BY_INDEX,TASK_CHANGE_BY_ID,TASK_CHANGE_BY_INDEX,
            TASK_COMPLETE_BY_INDEX,TASK_COMPLETE_BY_ID,TASK_BIND_TO_PROJECT,TASK_UNBIND_TO_PROJECT,
            TASK_SHOW_BY_PROJECT_ID,

            HELP, EXIT
    };

    @Override
    public Command[] getTerminalCommands(){
        return TERMINAL_COMMANDS;
    }

}
