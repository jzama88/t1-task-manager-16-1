package com.t1.alieva.tm.exception.system;

public final class CommandNotSupportedException extends AbstractSystemException{

    public CommandNotSupportedException(){
        super("Error! Command is incorrect...");
    }
}
