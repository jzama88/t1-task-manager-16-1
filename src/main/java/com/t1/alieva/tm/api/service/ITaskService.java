package com.t1.alieva.tm.api.service;

import com.t1.alieva.tm.enumerated.Sort;
import com.t1.alieva.tm.enumerated.Status;
import com.t1.alieva.tm.exception.entity.AbstractEntityNotFoundException;
import com.t1.alieva.tm.exception.field.AbstractFieldException;
import com.t1.alieva.tm.model.Project;
import com.t1.alieva.tm.model.Task;

import java.util.Comparator;
import java.util.List;

public interface ITaskService {

    List<Task> findAll();

    List<Task> findAll(Sort sort);

    List<Task> findAll(Comparator comparator);

    Task add(Task task) throws AbstractEntityNotFoundException;

    Task create(String name) throws AbstractFieldException, AbstractEntityNotFoundException;

    Task create(String name, String description) throws AbstractFieldException, AbstractEntityNotFoundException;

    void clear();

    Task findOneById(String id) throws AbstractFieldException;

    Task findOneByIndex(Integer index) throws AbstractFieldException;

    Task updateById(String id, String name, String description) throws AbstractFieldException, AbstractEntityNotFoundException;

    Task updateByIndex(Integer index, String name, String description) throws AbstractFieldException, AbstractEntityNotFoundException;

    void remove(Task task) throws AbstractEntityNotFoundException;

    Task removeById(String id) throws AbstractFieldException;

    Task removeByIndex(Integer index) throws AbstractFieldException;

    Task changeTaskStatusByIndex(Integer index, Status status) throws AbstractFieldException, AbstractEntityNotFoundException;

    Task changeTaskStatusById(String id, Status status) throws AbstractFieldException;

    List<Task> findAllByProjectId(String projectId);

}
